var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM color;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(color_id, callback) {
    var query = 'SELECT * FROM color WHERE color_id = ?';
    var queryData = [color_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE color
    var query = 'INSERT INTO color (color) VALUES (?)';

    var queryData = [params.color];

    connection.query(query, params.color, function(err, result) {

        // THEN USE THE color_id RETURNED AS insertId AND THE SELECTED ADDRESSes_IDs INTO color_address
        var color_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO color_address (color_id, addresses_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var colorAddressData = [];
        if (params.addresses_id.constructor === Array) {
            for (var i = 0; i < params.addresses_id.length; i++) {
                colorAddressData.push([color_id, params.addresses_id[i]]);
            }
        }
        else {
            colorAddressData.push([color_id, params.addresses_id]);
        }

        // NOTE THE EXTRA [] AROUND colorAddressData
        connection.query(query, [colorAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(color_id, callback) {
    var query = 'DELETE FROM color WHERE color_id = ?';
    var queryData = [color_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var colorAddressInsert = function(color_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO color_address (color_id, addresses_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var colorAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            colorAddressData.push([color_id, addressIdArray[i]]);
        }
    }
    else {
        colorAddressData.push([color_id, addressIdArray]);
    }
    connection.query(query, [colorAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.colorAddressInsert = colorAddressInsert;

//declare the function so it can be used locally
var colorAddressDeleteAll = function(color_id, callback){
    var query = 'DELETE FROM color_address WHERE color_id = ?';
    var queryData = [color_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.colorAddressDeleteAll = colorAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE color SET color = ? WHERE color_id = ?';
    var queryData = [params.color, params.color_id];

    connection.query(query, queryData, function(err, result) {
        //delete color_address entries for this color
        colorAddressDeleteAll(params.color_id, function(err, result){

            if(params.addresses_id != null) {
                //insert color_address ids
                colorAddressInsert(params.color_id, params.addresses_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(color_id, callback) {
    var query = 'CALL color_getinfo(?)';
    var queryData = [color_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};