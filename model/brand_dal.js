var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM brand;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(brand_id, callback) {
    var query = 'SELECT c.*, a.street, a.zip_code FROM brand c ' +
        'LEFT JOIN brand_address ca on ca.brand_id = c.brand_id ' +
        'LEFT JOIN addresses a on a.addresses_id = ca.addresses_id ' +
        'WHERE c.brand_id = ?';
    var queryData = [brand_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE brand
    var query = 'INSERT INTO brand (brand_name) VALUES (?)';

    var queryData = [params.brand_name];

    connection.query(query, params.brand_name, function(err, result) {

        // THEN USE THE brand_id RETURNED AS insertId AND THE SELECTED ADDRESSes_IDs INTO brand_address
        var brand_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO brand_address (brand_id, addresses_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var brandAddressData = [];
        if (params.addresses_id.constructor === Array) {
            for (var i = 0; i < params.addresses_id.length; i++) {
                brandAddressData.push([brand_id, params.addresses_id[i]]);
            }
        }
        else {
            brandAddressData.push([brand_id, params.addresses_id]);
        }

        // NOTE THE EXTRA [] AROUND brandAddressData
        connection.query(query, [brandAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(brand_id, callback) {
    var query = 'DELETE FROM brand WHERE brand_id = ?';
    var queryData = [brand_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var brandAddressInsert = function(brand_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO brand_address (brand_id, addresses_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var brandAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            brandAddressData.push([brand_id, addressIdArray[i]]);
        }
    }
    else {
        brandAddressData.push([brand_id, addressIdArray]);
    }
    connection.query(query, [brandAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.brandAddressInsert = brandAddressInsert;

//declare the function so it can be used locally
var brandAddressDeleteAll = function(brand_id, callback){
    var query = 'DELETE FROM brand_address WHERE brand_id = ?';
    var queryData = [brand_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.brandAddressDeleteAll = brandAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE brand SET brand_name = ? WHERE brand_id = ?';
    var queryData = [params.brand_name, params.brand_id];

    connection.query(query, queryData, function(err, result) {
        //delete brand_address entries for this brand
        brandAddressDeleteAll(params.brand_id, function(err, result){

            if(params.addresses_id != null) {
                //insert brand_address ids
                brandAddressInsert(params.brand_id, params.addresses_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

exports.edit = function(brand_id, callback) {
    var query = 'CALL brand_getinfo(?)';
    var queryData = [brand_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};