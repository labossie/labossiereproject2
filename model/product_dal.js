var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM product;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(product_id, callback) {
    var query = 'SELECT b.brand_name, t.type, c.color, p.name_color, p.price, p.cost, p.quantity '+
        'FROM product p ' +
        'join brand b on p.brand_id = b.brand_id ' +
        'join type t on p.type_id = t.type_id ' +
        'join color c on p.color_id = c.color_id ' +
        'WHERE product_id = ?';
    var queryData = [product_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete = function(product_id, callback) {
    var query = 'DELETE FROM product WHERE product_id = ?';
    var queryData = [product_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var productAddressDeleteAll = function(product_id, callback){
    var query = 'DELETE FROM product_addresses WHERE product_id = ?';
    var queryData = [product_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.productAddressDeleteAll = productAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE product SET brand_id = ? WHERE product_id = ?';
    var queryData = [params.brand_id, params.product_id];

    connection.query(query, queryData, function(err, result) {
        //delete product_address entries for this product
        productAddressDeleteAll(params.product_id, function(err, result){

            if(params.addresses_id != null) {
                //insert product_address ids
                productAddressInsert(params.product_id, params.addresses_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};
