var express = require('express');
var router = express.Router();
var product_dal= require('../model/product_dal');
var addresses_dal = require('../model/addresses_dal');

// View All products
router.get('/all', function(req, res) {
    product_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('product/productViewAll', { 'result':result });
        }
    });

});

// View the product for the given id
router.get('/', function(req, res){
    if(req.query.product_id == null) {
        res.send('product_id is null');
    }
    else {
        product_dal.getById(req.query.product_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('product/productViewById', {'result': result});
            }
        });
    }
});


// Delete a brand for the given brand_id
router.get('/delete', function(req, res){
    if(req.query.product_id == null) {
        res.send('product_id is null');
    }
    else {
        product_dal.delete(req.query.product_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/product/all');
            }
        });
    }
});
module.exports = router;