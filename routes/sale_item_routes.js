var express = require('express');
var router = express.Router();
var sale_item_dal= require('../model/sale_item_dal');


// View All sale_item
router.get('/all', function(req, res) {
    sale_item_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('sale_item/sale_itemViewAll', { 'result':result });
        }
    });

});

// View the sale_item for the given id
router.get('/', function(req, res){
    if(req.query.sale_item_id == null) {
        res.send('sale_item_id is null');
    }
    else {
        sale_item_dal.getById(req.query.sale_item_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('sale_item/sale_itemViewById', {'result': result});
            }
        });
    }
});

// Return the add a new sale_item form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    sale_item_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('sale_item/sale_itemAdd', {'sale_item': result});
        }
    });
});

// View the sale_item for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.street == null) {
        res.send('Sale items must be provided.');
    }
    else if(req.query.sale_item_id == null) {
        res.send('At least one sale items must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        sale_item_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/sale_item/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.sale_item_id == null) {
        res.send('A sale item id is required');
    }
    else {
        sale_item_dal.edit(req.query.sale_item_id, function(err, result){
            res.render('sale_item/sale_itemUpdate', {sale_item: result[0][0], sale_item: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.sale_item_id == null) {
        res.send('A sale item id is required');
    }
    else {
        sale_item_dal.getById(req.query.sale_item_id, function(err, sale_item){
            sale_item_dal.getAll(function(err, sale_item) {
                res.render('sale_item/sale_itemUpdate', {sale_item: sale_item[0], sale_item: sale_item});
            });
        });
    }

});

router.get('/update', function(req, res) {
    sale_item_dal.update(req.query, function(err, result){
        res.redirect(302, '/sale_item/all');
    });
});

// Delete a sale_item for the given sale_item_id
router.get('/delete', function(req, res){
    if(req.query.sale_item_id == null) {
        res.send('sale_item_id is null');
    }
    else {
        sale_item_dal.delete(req.query.sale_item_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/sale_item/all');
            }
        });
    }
});

module.exports = router;