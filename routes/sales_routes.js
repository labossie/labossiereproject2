var express = require('express');
var router = express.Router();
var sales_dal= require('../model/sales_dal');


// View All sales
router.get('/all', function(req, res) {
    sales_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('sales/salesViewAll', { 'result':result });
        }
    });

});

// View the sales for the given id
router.get('/', function(req, res){
    if(req.query.sale_id == null) {
        res.send('sale_id is null');
    }
    else {
        sales_dal.getById(req.query.sale_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('sales/salesViewById', {'result': result});
            }
        });
    }
});

// Return the add a new sales form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    sales_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('sales/salesAdd', {'sales': result});
        }
    });
});

// View the sales for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.street == null) {
        res.send('Sales must be provided.');
    }
    else if(req.query.sale_id == null) {
        res.send('At least one sales must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        sales_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/sales/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.sale_id == null) {
        res.send('A sales id is required');
    }
    else {
        sales_dal.edit(req.query.sale_id, function(err, result){
            res.render('sales/salesUpdate', {sales: result[0][0], sales: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.sale_id == null) {
        res.send('A sales id is required');
    }
    else {
        sales_dal.getById(req.query.sale_id, function(err, sales){
            sales_dal.getAll(function(err, sales) {
                res.render('sales/salesUpdate', {sales: sales[0], sales: sales});
            });
        });
    }

});

router.get('/update', function(req, res) {
    sales_dal.update(req.query, function(err, result){
        res.redirect(302, '/sales/all');
    });
});

// Delete a sales for the given sale_id
router.get('/delete', function(req, res){
    if(req.query.sale_id == null) {
        res.send('sale_id is null');
    }
    else {
        sales_dal.delete(req.query.sale_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/sales/all');
            }
        });
    }
});

module.exports = router;