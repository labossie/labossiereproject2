var express = require('express');
var router = express.Router();
var brand_dal = require('../model/brand_dal');
var addresses_dal = require('../model/addresses_dal');


// View All brands
router.get('/all', function(req, res) {
    brand_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('brand/brandViewAll', { 'result':result });
        }
    });

});

// View the brand for the given id
router.get('/', function(req, res){
    if(req.query.brand_id == null) {
        res.send('brand_id is null');
    }
    else {
        brand_dal.getById(req.query.brand_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('brand/brandViewById', {'result': result});
            }
        });
    }
});

// Return the add a new brand form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    addresses_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('brand/brandAdd', {'addresses': result});
        }
    });
});

// View the brand for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.brand_name == null) {
        res.send('Brand Name must be provided.');
    }
    else if(req.query.addresses_id == null) {
        res.send('At least one address must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        brand_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/brand/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.brand_id == null) {
        res.send('A brand id is required');
    }
    else {
        brand_dal.edit(req.query.brand_id, function(err, result){
            res.render('brand/brandUpdate', {brand: result[0][0], addresses: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.brand_id == null) {
        res.send('A brand id is required');
    }
    else {
        brand_dal.getById(req.query.brand_id, function(err, brand){
            addresses_dal.getAll(function(err, addresses) {
                res.render('brand/brandUpdate', {brand: brand[0], addresses: addresses});
            });
        });
    }

});

router.get('/update', function(req, res) {
    brand_dal.update(req.query, function(err, result){
        res.redirect(302, '/brand/all');
    });
});

// Delete a brand for the given brand_id
router.get('/delete', function(req, res){
    if(req.query.brand_id == null) {
        res.send('brand_id is null');
    }
    else {
        /*brand_dal.deletebrandAddress(req.query.brand_id, function(err, result) {
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/brand/all');
            }
        });*/
        brand_dal.delete(req.query.brand_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/brand/all');
            }
        });
    }
});

module.exports = router;